package com.example.thugx.songlyricsapp;

import android.content.res.AssetManager;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by thugX on 1/27/2017.
 */

public class AssetUtil {

    public static List<Songs> loadSongs(AssetManager assets) {
        BufferedReader reader;
        StringBuilder builder;
        String line;
        String json;
        JSONArray data;

        try {
            reader = new BufferedReader(new InputStreamReader(assets.open("songs.json")));
            builder = new StringBuilder();

            while ((line = reader.readLine()) != null)
                builder.append(line);

            json = builder.toString();
        }
        catch (IOException exception) {
            exception.printStackTrace();
            return new ArrayList<>();
        }

        try {
            data = new JSONArray(json);
        }
        catch (JSONException exception) {
            return new ArrayList<>();
        }

        List<Songs> output = new ArrayList<>();

        for (int index = 0; index < data.length(); index++)
            output.add(new Songs(data.optJSONObject(index)));
        return output;
    }
}
