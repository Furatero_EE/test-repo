package com.example.thugx.songlyricsapp;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ListViewFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ListViewFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ListViewFragment extends Fragment {
    protected List<Songs> songs;
    protected List<Artists> artists;
    protected ListViewAdapter adapter;
    public ArrayList mergedList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_list_view, container, false);
        this.songs = AssetUtil.loadSongs(this.getActivity().getAssets());
        this.artists = AssetUtilForArtists.loadArtists(this.getActivity().getAssets());

        mergedList = new ArrayList(this.songs);
        mergedList.addAll(this.artists);

        this.adapter = new ListViewAdapter(this.mergedList);
        ListView songAndArtistslist = (ListView)root.findViewById(R.id.listDisplay);
        songAndArtistslist.setAdapter(adapter);

        return root;
    }
}
