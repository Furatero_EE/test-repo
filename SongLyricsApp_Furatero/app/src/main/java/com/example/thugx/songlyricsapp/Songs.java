package com.example.thugx.songlyricsapp;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Array;

/**
 * Created by thugX on 1/27/2017.
 */

public class Songs implements Parcelable {
    public String model;
    public int pk;
    public JSONObject fieldsObject;

    public String title;
    public int artist;
    public String album;
    public String producer;
    public String lyrics;

    public Songs(JSONObject object) {
        this.fromJSON(object);
    }

    private void fromJSON(JSONObject object) {
        this.model = object.optString("model");
        this.pk = object.optInt("pk");

        try {
            fieldsObject = object.getJSONObject("fields");

            title = fieldsObject.getString("title");
            artist = fieldsObject.getInt("artist");
            album = fieldsObject.getString("album");
            producer = fieldsObject.getString("producer");
            lyrics = fieldsObject.getString("lyrics");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected Songs(Parcel in) {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.model);
        dest.writeInt(this.pk);
        dest.writeString(this.title);
        dest.writeInt(this.artist);
        dest.writeString(this.album);
        dest.writeString(this.producer);
        dest.writeString(this.lyrics);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Songs> CREATOR = new Creator<Songs>() {
        @Override
        public Songs createFromParcel(Parcel in) {
            Songs songs = new Songs(in);
            songs.readFromParcel(in);
            return songs;
        }

        @Override
        public Songs[] newArray(int size) {
            return new Songs[size];
        }
    };

    private void readFromParcel(Parcel in) {
        this.model = in.readString();
        this.pk = in.readInt();
        this.title = in.readString();
        this.artist = in.readInt();
        this.album = in.readString();
        this.producer = in.readString();
        this.lyrics = in.readString();
    }
}
