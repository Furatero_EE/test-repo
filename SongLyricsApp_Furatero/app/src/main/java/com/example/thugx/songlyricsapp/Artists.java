package com.example.thugx.songlyricsapp;

import android.os.Parcel;
import android.os.Parcelable;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by thugX on 1/27/2017.
 */

public class Artists implements Parcelable {
    public String model;
    public int pk;
    public JSONObject fieldsObject;

    public String name;
    public String birth_name;
    public String birth_date;
    public int age;

    public Artists(JSONObject object) {
        this.fromJSON(object);
    }

    private void fromJSON(JSONObject object) {
        this.model = object.optString("model");
        this.pk = object.optInt("pk");
        try {
            fieldsObject = object.getJSONObject("fields");

            name = fieldsObject.getString("name");
            birth_name = fieldsObject.getString("birth_name");
            birth_date = fieldsObject.getString("birth_date");
            age = fieldsObject.getInt("age");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected Artists(Parcel in) {

    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.model);
        dest.writeInt(this.pk);
        dest.writeString(this.name);
        dest.writeString(this.birth_name);
        dest.writeString(this.birth_date);
        dest.writeInt(this.age);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Artists> CREATOR = new Creator<Artists>() {
        @Override
        public Artists createFromParcel(Parcel in) {
            Artists artists = new Artists(in);
            artists.readFromParcel(in);
            return artists;
        }

        @Override
        public Artists[] newArray(int size) {
            return new Artists[size];
        }
    };

    private void readFromParcel(Parcel in) {
        this.model = in.readString();
        this.pk = in.readInt();
        this.name = in.readString();
        this.birth_name = in.readString();
        this.birth_date = in.readString();
        this.age = in.readInt();
    }
}
