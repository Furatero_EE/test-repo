package com.example.thugx.songlyricsapp;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thugX on 1/27/2017.
 */

public class ListViewAdapter extends BaseAdapter {
    protected List<Songs> songs;
    protected List<Artists> artists;


    public ListViewAdapter(List<Songs> songs) {
        this.songs = songs;
    }

    @Override
    public int getCount() {
        return this.songs.size();
    }

    @Override
    public Songs getItem(int position) {
        return this.songs.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = parent.getContext();
        convertView = LayoutInflater.from(context).inflate(
                R.layout.adapter_list_songs, parent, false);

        ViewHolder holder = new ViewHolder();

        holder.songName = (TextView)convertView.findViewById(R.id.songName);
        holder.artistName = (TextView)convertView.findViewById(R.id.artistName);

        convertView.setTag(holder);

        holder = (ViewHolder)convertView.getTag();
        Songs songs = this.getItem(position);

        //Log.d(String.valueOf(songs.title), "song title");
        holder.songName.setText(songs.title); //song name retrieved from json array
        //holder.artistName.setText(artists.name); // artist name retrieved from json array

        return convertView;
    }

    private class ViewHolder {
        public TextView songName;
        public TextView artistName;
    }
}
